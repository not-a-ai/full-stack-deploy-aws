#!/bin/bash

sudo apt-get update
sudo apt-get upgrade -y

curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh get-docker.sh 

sudo mkdir /var/www 
sudo mkdir /var/www/html
sudo mkdir /var/www/html/reactapp

sudo apt install nginx

# Nome do arquivo de configuração
CONFIG_FILE="/etc/nginx/sites-available/reactapp"

# Conteúdo do arquivo de configuração
CONFIG_CONTENT="server {
    listen 80;
    server_name _;
    location / {
        root /var/www/html/reactapp; # Serve files from /var/www/html
        index index.html;
        try_files \$uri \$uri/ /index.html;
    }
}"

sudo systemctl start nginx
sudo systemctl enable nginx

sudo chown ec2-user /var/www/html && sudo chmod 755 /var/www/html
sudo chown ec2-user /var/www/html/reactapp && sudo chmod 755 /var/www/html/reactapp



curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash
sudo apt-get install gitlab-runner
sudo usermod -aG docker gitlab-runner

sudo gitlab-runner register \
--non-interactive \
--url https://gitlab.com/ \
--token glrt-2o9nHXP64Gxs7yUsQ8Dr \
--executor shell 
